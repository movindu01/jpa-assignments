package annotations.example;

import java.util.Arrays;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import annotations.entities.Author;
import annotations.entities.Book;


public class App {
	public static void main(String[] args) {
		
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("persistence-annotations");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		
		Book book1 = new Book("Book1", "description1", false);
		Book book2 = new Book("Book2", "description2", false);
		Book book3 = new Book("Book3", "description3", true);
		
		entityTransaction.begin();
		entityManager.persist(book1);
		entityManager.persist(book2);
		entityManager.persist(book3);
		
		Author author1 = new Author("John", Arrays.asList(book1, book2), "Colombo");
		Author author2 = new Author("Jane", Arrays.asList(book3), "Galle");
		entityManager.persist(author1);
		entityManager.persist(author2);
		entityTransaction.commit();

		entityManager.close();
		entityManagerFactory.close();
	}
}
